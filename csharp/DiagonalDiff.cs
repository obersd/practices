namespace practices
{
    using System;
    using System.Collections.Generic;

    internal static class DiagonalDiff
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(new List<List<int>> { new List<int> { 11, 2, 4 }, new List<int> { 4, 5, 6 }, new List<int> { 10, 8, -12 } })}");
            Console.WriteLine($"R={Solve(new List<List<int>> { new List<int> { 1, 2, 3 }, new List<int> { 4, 5, 6 }, new List<int> { 9, 8, 9 } })}");
        }

        private static int Solve(List<List<int>> arr)
        {
            var d1 = 0;
            var d2 = 0;
            int t;
            for (var i = 0; i < arr.Count; i++)
            {
                t = arr[i][i];
                if (-100 <= t && t <= 100)
                {
                    d1 += t;
                }

                t = arr[arr.Count - i - 1][i];
                if (-100 <= t && t <= 100)
                {
                    d2 += t;
                }
            }

            return Math.Abs(d1 - d2);
        }
    }
}