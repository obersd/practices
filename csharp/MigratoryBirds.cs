namespace practices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal static class MigratoryBirds
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(new List<int> {1, 1, 2, 2, 3})}");
            Console.WriteLine($"R={Solve(new List<int> {1, 4, 4, 4, 5, 3})}");
            Console.WriteLine($"R={Solve(new List<int> {1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4})}");
        }

        private static int Solve(List<int> arr)
        {
            var table = new Dictionary<int, int>()
            {
                {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}
            };

            foreach (var i in arr) table[i]++;
            var maxBirds = table.Where(a => a.Value == table.Max(b => b.Value));
            return maxBirds.Count() == 1 ? maxBirds.First().Key : maxBirds.Min(x => x.Key);
        }
    }
}