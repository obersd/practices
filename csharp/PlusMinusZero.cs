namespace practices
{
    using System;
    using System.Linq;

    internal static class PlusMinusZero
    {
        public static void Solution()
        {
            Solve(new[] { 1, 1, 0, -1, -1 });
            Solve(new[] { -4, 3, -9, 0, 4, 1 });
        }

        private static void Solve(int[] arr)
        {
            var n = arr.Length;
            var plus = arr.Count(x => x > 0);
            var zero = arr.Count(x => x == 0);
            var minus = n - plus - zero;
            Console.WriteLine($"{((decimal)plus / n):0.000000}");
            Console.WriteLine($"{((decimal)minus / n):0.000000}");
            Console.WriteLine($"{((decimal)zero / n):0.000000}");
        }
    }
}