namespace practices
{
    using System;
    using System.Linq;

    static class JumpClouds
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(new[] { 0, 0, 0, 1, 0, 0 })}");
            Console.WriteLine($"R={Solve(new[] { 0, 0, 1, 0, 0, 1, 0 })}");
            Console.WriteLine($"R={Solve(new[] { 0, 1, 0, 0, 0, 1, 0 })}");
            Console.WriteLine($"R={Solve(new[] { 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 })}");
        }

        private static int Solve(int[] c)
        {
            if (c.Length < 2 || c.Length > 100)
            {
                throw new ArgumentException("c.Length<2 || c.Length>100");
            }

            if (c[0] != 0 || c[^1] != 0)
            {
                throw new ArgumentException("c[0]!=0 or c[n-1]!=0");
            }

            if (c.Count(x => x == 0) + c.Count(x => x == 1) != c.Length)
            {
                throw new ArgumentException("array must be contains only items with value of 0 or 1");
            }

            var count = 0;
            var i = 0;
            while (i < c.Length)
            {
                if (i + 2 < c.Length && c[i + 2] == 0)
                {
                    count++;
                    i += 2;
                    continue;
                }

                if (i + 1 < c.Length && c[i + 1] == 0)
                {
                    count++;
                    i += 1;
                    continue;
                }

                i++;
            }

            return count;
        }
    }
}