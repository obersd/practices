namespace practices
{
    using System;
    using System.Collections.Generic;

    internal static class CompareTriplets
    {
        public static void Solution()
        {
            Console.WriteLine($"R={string.Join(",", Solve(new List<int> {1, 2, 3}, new List<int> {3, 2, 1}))}");
            Console.WriteLine($"R={string.Join(",", Solve(new List<int> {5, 6, 7}, new List<int> {3, 6, 10}))}");
            Console.WriteLine($"R={string.Join(",", Solve(new List<int> {17, 28, 30}, new List<int> {99, 16, 8}))}");
        }

        private static List<int> Solve(List<int> a, List<int> b)
        {
            var l = new List<int>(2) {0, 0};
            const int Alice = 0;
            const int Bob = 1;
            for (var i = 0; i < a.Count; i++)
            {
                var aItem = a[i];
                var bItem = b[i];
                if (aItem > bItem)
                {
                    l[Alice]++;
                }
                else if (aItem < bItem)
                {
                    l[Bob]++;
                }
            }

            return l;
        }
    }
}