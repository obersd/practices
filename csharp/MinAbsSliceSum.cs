namespace practices
{
    using System;

    /// <summary>
    /// A non-empty zero-indexed array A of N integers is given. A pair of integers (P,Q), such that 0 <= P <= Q < N, is called slice of array A.
    /// The sum of a slice (P,Q) is the total of A[P]+A[P+1]+...+A[Q].
    ///
    /// A min abs slice is a slice whose absolute sum is minimal.
    ///
    /// For example, array A such that:
    ///
    /// { 2, -4, 6, -3, 9 }
    ///
    ///
    /// (0,1) = |2 + (-4)| = |2 - 4| = |-2| = 2
    /// (0,3) = |2 + (-4) + 6 + (-3) = 1
    /// (1,3) = |2 + (-4)| = 1
    ///
    /// Both slices (0,1) and (1,3) are min abs slice and their absolute sum equals 1.
    /// </summary>
    internal static class MinAbsSliceSum
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(new[] { 2, -4, 6, -3, 9 })}");
        }

        private static int Solve(int[] a)
        {
            var n = a.Length;
            switch (n)
            {
                case 0: return 0;
                case 1: return Math.Abs(a[0]);
                case 2: return Math.Abs(a[0] + a[1]);
            }

            var sum = Math.Abs(a[0] + a[1]);
            int tmp;
            for (var i = 0; i < n; i++)
            {
                tmp = a[i];
                for (var j = i + 1; j < n - 1; j++)
                {
                    tmp += a[j];
                    tmp = Math.Abs(tmp);
                    if (tmp < sum)
                    {
                        sum = tmp;
                    }
                }
            }

            return sum;
        }
    }
}