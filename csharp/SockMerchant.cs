namespace practices
{
    using System;
    using System.Linq;

    internal static class SockMerchant
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(9, new[] { 10, 20, 20, 10, 10, 30, 50, 10, 20 })}");
            Console.WriteLine($"R={Solve(9, new[] { 6, 5, 2, 3, 5, 2, 2, 1, 1, 5, 1, 3, 3, 3, 5 })}");
        }

        private static int Solve(int n, int[] ar)
        {
            if (n < 1 || n > 100)
            {
                throw new IndexOutOfRangeException(nameof(n));
            }

            if (ar.Any(i => i <= 0 || i > 100))
            {
                throw new IndexOutOfRangeException(nameof(ar));
            }

            return ar.GroupBy(item => item).Select(x => x.Count() / 2).Sum(ints => ints);
        }
    }
}