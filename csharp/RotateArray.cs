﻿namespace practices
{
    using System;

    internal static class RotateArray
    {
        public static void Solution()
        {
            Console.WriteLine($"{string.Join(",", Solve2(new[] { 1, 2, 3, 4, 5 }, 4))}");
            Console.WriteLine($"{string.Join(",", Solve2(new[] { 41, 73, 89, 7, 10, 1, 59, 58, 84, 77, 77, 97, 58, 1, 86, 58, 26, 10, 86, 51 }, 10))}");
            Console.WriteLine($"{string.Join(",", Solve2(new[] { 33, 47, 70, 37, 8, 53, 13, 93, 71, 72, 51, 100, 60, 87, 97 }, 13))}");
        }

        private static int[] Solve2(int[] a, int d)
        {
            if (a.Length < 1 || a.Length > Math.Pow(10, 5))
            {
                throw new ArgumentException("1 <= a.Length <= 10^5");
            }

            if (d < 1 || d > a.Length)
            {
                throw new ArgumentException("1 <= d <= a.Length");
            }

            if (d == a.Length)
            {
                return a;
            }

            var result = new int[a.Length];
            for (var i = 0; i < a.Length; i++)
            {
                if (i - d < 0)
                {
                    result[a.Length + i - d] = a[i];
                }
                else
                {
                    result[i - d] = a[i];
                }
            }

            return result;
        }
    }
}