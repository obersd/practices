namespace practices
{
    using System;

    internal static class DivSumPairs
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(6, 3, new[] {1, 3, 2, 6, 1, 2})}");
        }

        private static int Solve(int n, int k, int[] ar)
        {
            if (!(2 <= n && n <= 100)) return 0;
            int pair1;
            int pair2;
            var count = 0;
            for (var i = 0; i < ar.Length; i++)
            {
                pair1 = ar[i];
                if (!(1 <= pair1 && pair1 <= 100)) continue;
                for (var j = i + 1; j < ar.Length; j++)
                {
                    pair2 = ar[j];
                    if (!(1 <= pair2 && pair2 <= 100)) continue;
                    if ((pair1 + pair2) % k == 0)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
    }
}