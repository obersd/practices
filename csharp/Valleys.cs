namespace practices
{
    using System;
    using System.Linq;

    internal static class Valleys
    {

        public static void Solution()
        {
            Console.WriteLine($"R={Solve(8, "DDUUUUDD")}");
            Console.WriteLine($"R={Solve(8, "DDUUUUDD")}");
        }

        private static int Solve(int steps, string path)
        {
            var r = 0;
            var k = 0;
            if (path.Count(x => x == 'U' || x == 'D') != path.Length || path.Length < 2 || steps != path.Length)
                throw new ArgumentException("Check parameters");
            
            foreach (var c in path)
            {
                if (c == 'U') r++;
                else if (c == 'D')
                {
                    r--;
                    if (r == 0) continue;
                }

                if (r == 0) k++;
            }
            return k;
        }
    }
}