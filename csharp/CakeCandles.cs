namespace practices
{
    using System;
    using System.Collections.Generic;

    internal static class CakeCandles
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve(new List<int> {4, 4, 1, 3})}");
            Console.WriteLine($"R={Solve(new List<int> {3, 2, 1, 3})}");
        }
        private static int Solve(List<int> candles)
        {
            var count = 0;
            var v = 0;
            foreach (var c in candles)
            {
                if (c < 1 || c > 10_000_000)
                {
                    continue;
                }

                if (c > v)
                {
                    v = c;
                    count = 1;
                }
                else if (c == v)
                {
                    count++;
                }
            }

            return count;
        }
    }
}