namespace practices
{
    using System;
    using System.Text;

    internal static class DecryptPass
    {
        public static void Solution()
        {
            // 51Pa*0Lp*0e
            // Pa*1Lp*5e
            // aP1pL5e
            Console.WriteLine($"{Solve("51Pa*0Lp*0e")}");
            Console.WriteLine($"{Solve("pTo*Ta*O")}");
        }

        private static string Solve(string s)
        {
            char[] digits = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            var sb = new StringBuilder(s);
            for (var index = sb.Length - 1; index >= 0; index--)
            {
                var curChar = sb[index];
                if (char.IsDigit(curChar) && curChar == '0')
                {
                    var str = sb.ToString();
                    var firstPos = str.IndexOfAny(digits, 0);
                    var lastPos = str.LastIndexOf('0');
                    sb.Insert(lastPos, sb[firstPos].ToString());
                    sb.Remove(lastPos + 1, 1);
                    sb.Remove(0, 1);
                }
                else if (curChar == '*')
                {
                    var startIndexForRemoval = index - 2;
                    var swapChar1 = sb[index - 1];
                    var swapChar2 = sb[startIndexForRemoval];
                    sb.Remove(startIndexForRemoval, 3);
                    sb.Insert(startIndexForRemoval, new[] { swapChar1, swapChar2 });
                }
            }

            return sb.ToString();
        }

        private static string OlderSolver (string s)
        {
            var sb = new StringBuilder(s);
            for (var index = sb.Length - 1; index >= 0; index--)
            {
                var curChar = sb[index];
                if (char.IsDigit(curChar) && curChar == '0')
                {
                    var str = sb.ToString();
                    var firstPos = str.IndexOfAny(new[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' }, 0);
                    var lastPos = str.LastIndexOf('0');
                    sb.Insert(lastPos, sb[firstPos].ToString());
                    sb.Remove(lastPos + 1, 1);
                    sb.Remove(0, 1);
                }
                else if (curChar == '*')
                {
                    var d1 = index - 1;
                    var d2 = index - 2;
                    var c1 = sb[d1];
                    var c2 = sb[d2];
                    sb.Remove(index, 1);
                    sb.Remove(d1, 1);
                    sb.Remove(d2, 1);
                    sb.Insert(d2, c1);
                    sb.Insert(d1, c2);
                }
            }

            return sb.ToString();
        }
    }
}