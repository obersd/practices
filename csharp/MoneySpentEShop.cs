namespace practices
{
    using System;

    /// <summary>
    /// More expensive option
    /// </summary>
    internal static class MoneySpentEShop
    {
        public static void Solution()
        {
            Console.Write($"R={Solve(new[] { 40, 50, 60 }, new[] { 5, 8, 12 }, 60)}");
            Console.Write($"R={Solve(new[] { 3, 1 }, new[] { 5, 2, 8 }, 10)}");
            Console.Write($"R={Solve(new[] { 4 }, new[] { 5 }, 5)}");
        }

        private static int Solve(int[] keyboards, int[] drives, int b)
        {
            const int Max = 1_000_000;
            var r = -1;
            var t = 0;
            foreach (var priceKeyboard in keyboards)
            {
                if (priceKeyboard < 1 || priceKeyboard > Max)
                {
                    continue;
                }

                foreach (var priceDrives in drives)
                {
                    if (priceDrives < 1 || priceDrives > Max)
                    {
                        continue;
                    }

                    t = priceKeyboard + priceDrives;
                    if (t > r && t <= b)
                    {
                        r = t;
                    }
                }
            }

            return r <= b ? r : -1;
        }
    }
}