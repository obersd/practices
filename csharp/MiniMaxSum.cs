namespace practices
{
    using System;
    using System.Linq;

    internal static class MiniMaxSum
    {
        public static void Solution()
        {
            Solve(new[] { 1, 3, 5, 7, 9 });
            Solve(new[] { 1, 2, 3, 4, 5 });
        }

        private static void Solve(int[] arr)
        {
            var x = arr.Max();
            var lst = arr.Select(x => (long)x).Where(x => x >= 1 && x <= 1_000_000_000).ToList();
            lst.Sort();
            Console.WriteLine($"{lst.Take(4).Sum()} {lst.Skip(1).Take(4).Sum()}");
        }
    }
}