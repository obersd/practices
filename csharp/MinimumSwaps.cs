namespace practices
{
    using System;

    internal static class MinimumSwaps
    {
        public static void Solution()
        {
            //Console.WriteLine($"{minimumSwaps2(new[] { 4, 3, 1, 2 })}"); // 3
            //Console.WriteLine($"{minimumSwaps2(new[] { 2, 3, 4, 1, 5 })}"); // 3
            Console.WriteLine($"{Solve(new[] { 7, 1, 3, 2, 4, 5, 6 })}"); // 5
            //Console.Write($"{minimumSwaps2(new[] { 1, 3, 5, 2, 4, 6, 7 })}"); //3
        }

        private static int Solve(int[] a)
        {
            int swap = 0;
            for (int i = 0; i < a.Length; i++)
            {
                var real = i + 1;
                var y = a[i];
                if (real != y)
                {
                    int t = i;
                    while (a[t] != real)
                    {
                        t++;
                    }
                    int temp = a[t];
                    a[t] = a[i];
                    a[i] = temp;
                    swap++;
                }
            }
            return swap;

        }

        static int minimumSwaps(int[] arr)
        {
            var i = 0;
            var count = 0;
            var temp = 0;
            while (i < arr.Length)
            {
                if (arr[i] != i + 1)
                {
                    temp = arr[i];
                    arr[i] = arr[temp - 1];
                    arr[temp - 1] = temp;
                    count++;
                }
                else
                {
                    i++;
                }
            }

            return count;
        }
    }
}