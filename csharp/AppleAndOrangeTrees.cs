namespace practices
{
    using System;

    internal static class AppleAndOrangeTrees
    {
        public static void Solution()
        {
            Solve(7, 11, 5, 15, new[] {-2, 2, 1}, new[] {5, -6});
        }

        private static void Solve(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            Func<int, bool> inRangeFunc = i => -100000 <= i && i <= 100000;
            var applesInRange = 0;
            var orangesInRange = 0;
            var distance = 0;
            foreach (var t1 in apples)
            {   
                distance = a + t1;
                if (!inRangeFunc(distance))
                {
                    continue;
                }

                if (distance >= s && distance <= t)
                {
                    applesInRange++;
                }
            }

            foreach (var t1 in oranges)
            {
                distance = b + t1;
                if (!inRangeFunc(distance))
                {
                    continue;
                }
                if (distance >= s && distance <= t)
                {
                    orangesInRange++;
                }
            }

            Console.WriteLine($"{applesInRange}");
            Console.WriteLine($"{orangesInRange}");
        }
    }
}