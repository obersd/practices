namespace practices
{
    using System;
    using System.Linq;

    internal static class RepeatString
    {
        public static void Solution()
        {
            Console.WriteLine($"R={Solve("abcac", 10)}");
            Console.WriteLine($"R={Solve("a", 100)}");
            Console.WriteLine($"R={Solve("aba", 10)}");
            Console.WriteLine($"R={Solve("a", 1000000000000)}");
        }

        private static long Solve(string s, long n)
        {
            Func<string, long, long> lengthEqualN = (str, _) => str.LongCount(x => x == 'a');
            Func<string, long, long> lengthGreaterThanN = (str, m) => str.Substring(0, (int)m).LongCount(x => x == 'a');

            if (s.Length == n)
            {
                return lengthEqualN(s, 0);
            }

            if (s.Length > n)
            {
                return lengthGreaterThanN(s, n);
            }

            var remainder = n % s.Length;
            return lengthGreaterThanN(s, remainder) + (lengthEqualN(s, 0) * (n - remainder) / s.Length);
        }
    }
}