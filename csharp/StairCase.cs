namespace practices
{
    using System;
    using System.Text;

    internal static class StairCase
    {
        public static void Solution()
        {
            Solve(4);
        }

        private static void Solve(int n)
        {
            var sb = new StringBuilder();
            for (var i = n - 1; i >= 0; i--)
            {
                sb = sb.AppendLine(string.Empty.PadLeft(n - i, '#').PadLeft(n, ' '));
            }

            Console.WriteLine(sb);
        }
    }
}