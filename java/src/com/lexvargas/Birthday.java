package com.lexvargas;

import java.util.Arrays;
import java.util.List;

public final class Birthday {
    public static void solution() {
        System.out.println("R=" + solve(Arrays.asList(1, 2, 1, 3, 2), 3, 2));
        System.out.println("R=" + solve(Arrays.asList(1, 1, 1, 1, 1, 1), 3, 2));
        System.out.println("R=" + solve(Arrays.asList(4, 1), 4, 1));
    }

    private static int solve(List<Integer> s, int d, int m) {
        int c = 0;
        for (int i = 0; i < s.size() - m; i++) {
            if (d == sum(s, i, m)) {
                c++;
            }
        }
        return c;
    }

    private static int sum(List<Integer> list, int start, int length) {
        int sum = 0;
        for (int i = start; i < start + length; i++) {
            sum += list.get(i);
        }

        return sum;
    }
}
