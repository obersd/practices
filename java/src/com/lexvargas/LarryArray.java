package com.lexvargas;

public final class LarryArray {

    public static void solution() {
        System.out.println("R=" + solve(new int[]{1, 6, 5, 2, 4, 3}));
        System.out.println("R=" + solve(new int[]{3, 1, 2}));
        System.out.println("R=" + solve(new int[]{1, 3, 4, 2}));
        System.out.println("R=" + solve(new int[]{1, 2, 3, 5, 4}));
    }

    private static String solve(int[] a) {
        for(int i = 0; i < a.length; i++){
            for(int j = a.length-3; j >=i; j--){
                while(a[j] > a[j+1] || a[j] > a[j+2]){
                    rotate(a, j);
                }
            }
        }
        return a[a.length - 2] < a[a.length - 1] ? "YES" : "NO";
    }

    private static void rotate(int[] a, int i) {
        int t = a[i];
        a[i] = a[i + 1];
        a[i + 1] = a[i + 2];
        a[i + 2] = t;
    }


    private static boolean isSorted(int[] a, int pos) {
        if (pos == 0 || pos == 1) {
            return true;
        }

        if (a[pos - 1] < a[pos - 2]) {
            return false;
        }

        return isSorted(a, pos - 1);
    }
}