package com.lexvargas;

import java.util.Arrays;
import java.util.List;

public final class Between2Sets {

    public static void solution() {
        System.out.println("R=" + solve(Arrays.asList(2, 6), Arrays.asList(24, 36)));
        System.out.println("R=" + solve(Arrays.asList(2, 4), Arrays.asList(16, 32, 96)));
    }

    private static int solve(List<Integer> a, List<Integer> b) {
        int mcm = mcm(a.toArray(new Integer[]{}));
        int mcd = mcd(b.toArray(new Integer[]{}));
        int c = 0;
        for (int i = mcm, j = 2; i <= mcd; i = mcm * j, j++) {
            if (mcd % i == 0) {
                c++;
            }
        }

        return c;
    }

    private static int mcd(int a, int b) {
        int tmp;
        while (b > 0) {
            tmp = b;
            b = a % b;
            a = tmp;
        }
        return a;
    }

    private static int mcd(Integer[] arr) {
        int result = arr[0];
        for (int i = 1; i < arr.length; i++) {
            result = mcd(result, arr[i]);
        }
        return result;
    }

    private static int mcm(int a, int b) {
        return a * (b / mcd(a, b));
    }

    private static int mcm(Integer[] arr) {
        int result = arr[0];
        for (int i = 1; i < arr.length; i++) {
            result = mcm(result, arr[i]);
        }
        return result;
    }
}
