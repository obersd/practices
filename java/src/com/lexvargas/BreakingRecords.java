package com.lexvargas;

public final class BreakingRecords {

    public static void solution() {
        int[] x1 = solve(new int[]{10, 5, 20, 20, 4, 5, 2, 25, 1});
        int[] x2 = solve(new int[]{3, 4, 21, 36, 10, 28, 35, 5, 24, 42});
    }

    private static int[] solve(int[] scores) {
        int min = scores[0];
        int max = scores[0];
        int cmax = 0;
        int cmin = 0;
        for (int score : scores) {
            if (score > max) {
                max = score;
                cmax++;
            }

            if (score < min) {
                min = score;
                cmin++;
            }
        }

        return new int[]{cmax, cmin};
    }
}
