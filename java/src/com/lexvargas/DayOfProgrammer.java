package com.lexvargas;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public final class DayOfProgrammer {
    public static void solution() {
        System.out.println(solve(1916));
        System.out.println(solve(1984));
        System.out.println(solve(2017));
        System.out.println(solve(2016));
        System.out.println(solve(1800));
    }

    private static String solve(int year) {
        if (year < 1918) {
            return year % 4 == 0 ? "12.09." + year : "13.09." + year;
        } else if (year == 1918) {
            return "26.09." + year;
        } else {
            return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0 ? "12.09." + year : "13.09." + year;
        }
    }
}
