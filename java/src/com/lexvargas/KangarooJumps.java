package com.lexvargas;

public final class KangarooJumps {

    public static void solution() {
        System.out.println("R=" + solve(0, 2, 5, 3));
        System.out.println("R=" + solve(0, 3, 4, 2));
        System.out.println("R=" + solve(21, 6, 47, 3));
        System.out.println("R=" + solve(4523, 8092, 9419, 8076));
        System.out.println("R=" + solve(43, 2, 70, 2));
    }

    private static String solve(int x1, int v1, int x2, int v2) {
        final int Top = 10_000;
        if ( !(1 <= v1 && v1 <= Top) || !(1 <= v2 && v2 <= Top))
        {
            return "NO";
        }

        if (!(0 <= x1 && x1 <= x2 && x2 <= Top)) {
            return "NO";
        }

        if (x2 > x1 && v2 >= v1) {
            return "NO";
        }

        for (int i = 0; i < Top; i++) {
            if ((x2 - x1) % (v1 - v2) == 0) {
                return "YES";
            }

            x1 += v1;
            x2 += v2;
        }

        return "NO";
    }
}
